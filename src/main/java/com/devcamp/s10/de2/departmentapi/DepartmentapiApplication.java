package com.devcamp.s10.de2.departmentapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DepartmentapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DepartmentapiApplication.class, args);
	}

}
