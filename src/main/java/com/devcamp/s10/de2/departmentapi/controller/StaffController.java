package com.devcamp.s10.de2.departmentapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10.de2.departmentapi.models.Department;
import com.devcamp.s10.de2.departmentapi.models.Staff;
import com.devcamp.s10.de2.departmentapi.services.DepartmentService;
import com.devcamp.s10.de2.departmentapi.services.StaffService;

@RestController
@CrossOrigin
@RequestMapping("/")

public class StaffController {
     private StaffService staffService;;

     public StaffController(StaffService staffService){
          this.staffService = staffService;
     }

     @GetMapping("/staff")
     public ArrayList<Staff> getAllStaffs(){
          return staffService.getAllStaff();
     }

     @GetMapping("/age/{age}")
     public ArrayList<Staff> getStaffOlderThanAge(@PathVariable int age){
          return staffService.getStaffOlderThanAge(age);
     }

     @GetMapping("/average-age/{averageAge}")
    public ArrayList<Department> getDepartmentsWithAverageAgeGreaterThan(@PathVariable float averageAge){
     return staffService.getDepartmentsWithAverageAgeGreaterThan(averageAge);
    }
}
