package com.devcamp.s10.de2.departmentapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10.de2.departmentapi.models.Department;
import com.devcamp.s10.de2.departmentapi.services.DepartmentService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class DepartmentController {
    private DepartmentService departmentService;
    
    public DepartmentController(DepartmentService departmentService){
     this.departmentService = departmentService;
    }

    @GetMapping("/department")
    public ArrayList<Department> getAllDepartment(){
          return departmentService.getAllDepartments();
    }

    @GetMapping("/department/{departmentId}")
    public Department getDepartmentById(@PathVariable int departmentId){
     return departmentService.getDepartmentById(departmentId);
    }

    

}
