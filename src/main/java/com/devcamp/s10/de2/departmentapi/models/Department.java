package com.devcamp.s10.de2.departmentapi.models;

import java.util.ArrayList;

public class Department {
     private int id;
     private String name;
     private String address;
     private ArrayList<Staff> staffs;
     
     public Department() {
     }

     public Department(int id, String name, String address) {
          this.id = id;
          this.name = name;
          this.address = address;
     }

     public Department(int id, String name, String address, ArrayList<Staff> staffs) {
          this.id = id;
          this.name = name;
          this.address = address;
          this.staffs = staffs;
     }

     public int getId() {
          return id;
     }

     public void setId(int id) {
          this.id = id;
     }

     public String getName() {
          return name;
     }

     public void setName(String name) {
          this.name = name;
     }

     public String getAddress() {
          return address;
     }

     public void setAddress(String address) {
          this.address = address;
     }

     public ArrayList<Staff> getStaffs() {
          return staffs;
     }

     public void setStaffs(ArrayList<Staff> staffs) {
          this.staffs = staffs;
     }
     public void addStaff(Staff staff) {
        if (staffs == null) {
            staffs = new ArrayList<>();
        }
        staffs.add(staff);
    }

     public float getAverageAge(){
          int totalAge = 0;
        for (Staff staff : staffs) {
            totalAge += staff.getAge();
        }
        if (staffs.size() > 0) {
            return (float) totalAge / staffs.size();
        } else {
            return 0;
        }
    }

}
