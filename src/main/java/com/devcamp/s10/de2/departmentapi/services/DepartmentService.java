package com.devcamp.s10.de2.departmentapi.services;

import java.util.ArrayList;


import org.springframework.stereotype.Service;


import com.devcamp.s10.de2.departmentapi.models.Department;
import com.devcamp.s10.de2.departmentapi.models.Staff;

@Service
public class DepartmentService {
     private ArrayList<Department> departments;

    public DepartmentService() {
        departments = new ArrayList<>();

        Department department1 = new Department(1, "Department 1", "tang 1");
        department1.addStaff(new Staff(1, "Staff 1", 10));
        department1.addStaff(new Staff(2, "Staff 2", 20));
        department1.addStaff(new Staff(3, "Staff 3", 30));

        Department department2 = new Department(2, "Department 2", "tang 2");
        department2.addStaff(new Staff(4, "Staff 4", 40));
        department2.addStaff(new Staff(5, "Staff 5", 50));
        department2.addStaff(new Staff(6, "Staff 6", 60));

        Department department3 = new Department(3, "Department 3", "tang 3");
        department3.addStaff(new Staff(7, "Staff 7", 70));
        department3.addStaff(new Staff(8, "Staff 8", 80));
        department3.addStaff(new Staff(9, "Staff 9", 90));

        departments.add(department1);
        departments.add(department2);
        departments.add(department3);
    }

    public ArrayList<Department> getAllDepartments() {
        return departments;
    }

    public Department getDepartmentById(int departmentId) {
        for (Department department : departments) {
            if (department.getId() == departmentId) {
                return department;
            }
        }
        return null; // Xử lý khi không tìm thấy departmentId
    }

    

}
